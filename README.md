# Expense application

This project is created on react.

## How to setup on local env 

___

## To run JSON server

Install dependencies using below command.

### `cd server`

### `npm install`

Once dependencies are installed run the project using below command

### `npm start`

---
## To run the main application

Install dependencies using below command.

### `cd expenseapp`

### `npm install`

Once dependencies are installed run the project using below command

### `npm start`




